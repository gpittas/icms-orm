#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from sqlalchemy.types import TypeDecorator, String
from datetime import datetime


class StringInt(TypeDecorator):
    impl = None

    def __init__(self, *arg, **kw):
        TypeDecorator.__init__(self, *arg, **kw)
        self.impl = String(kw['length'])

    def process_bind_param(self, value, dialect):
        if isinstance(value, str):
            return value
        return str(value or 0)

    def process_result_value(self, value, dialect):
        if value is not None and len(value.strip()) > 0:
            return int(value)
        return 0


class StringFloat(TypeDecorator):
    impl = None

    def __init__(self, *arg, **kw):
        TypeDecorator.__init__(self, *arg, **kw)
        self.impl = String(kw['length'])

    def process_bind_param(self, value, dialect):
        if isinstance(value, str):
            return value
        return str(value or 0.0)

    def process_result_value(self, value, dialect):
        if isinstance(value, float):
            return value
        if value is not None and len(value.strip()) > 0:
            return float(value)
        return 0.0


class StringBool(TypeDecorator):
    impl = String(1)

    def process_bind_param(self, value, dialect):
        if isinstance(value, str):
            return value
        return str(int(value or False))

    def process_result_value(self, value, dialect):
        if value in ['0', '1']:
            return bool(int(value))
        return False


class StringDatetime(TypeDecorator):
    impl = String(50)
    format = '%Y/%m/%d %H:%M'
    custom_format = None

    def __init__(self, *arg, **kw):
        self.custom_format = kw.pop('format', self.format)
        self.impl = String(kw.pop('length', 50))
        TypeDecorator.__init__(self, *arg, **kw)

    def process_bind_param(self, value, dialect):
        if isinstance(value, str):
            return value
        if value is None:
            return None
        return value.strftime(format=self.custom_format)

    def process_result_value(self, value, dialect):
        try:
            if value is None or str(value) == '':
                return None
            assert isinstance(self.custom_format, str)
            return datetime.strptime(value, self.custom_format)
        except ValueError:
            return None


class StringDate(StringDatetime):

    format = '%Y/%m/%d'

    def __init__(self, *arg, **kw):
        super().__init__(self, *arg, **kw)

    def process_result_value(self, value, dialect):
        return super().process_result_value(value, dialect).date()
