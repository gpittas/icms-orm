"""table for cms award

Revision ID: 2021.03
Revises: 2021.02
Create Date: 2021-07-03 17:15:47.715448

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2021.03'
down_revision = '2021.02'
branch_labels = None
depends_on = None


def upgrade():
	op.create_table('award_nominee',
    sa.Column('nominee_id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('cms_id', sa.Integer(), nullable=True),
    sa.Column('hrd_id', sa.Integer(), nullable=True),
    sa.Column('name', sa.String(length=128), nullable=False),
    sa.Column('organization', sa.String(length=200), nullable=True),
    sa.Column('subsystem', sa.String(length=200), nullable=True),
    sa.ForeignKeyConstraint(['cms_id'],[u'toolkit.person.cms_id'], name=op.f('fk_award_nominee_cms_id_person'), onupdate='CASCADE', ondelete='SET NULL'),
    sa.PrimaryKeyConstraint('nominee_id', name=op.f('pk_award_nominee')),
    schema='toolkit'
    )
	op.create_table('award_type',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('type', sa.String(length=200), nullable=False),
    sa.Column('description', sa.String(length=1000), nullable=False),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_award_type')),
    schema='toolkit'
    )
	op.create_table('award',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('award_type_id', sa.Integer(), nullable=True),
    sa.Column('title', sa.String(length=200), nullable=False),
    sa.Column('status', sa.String(length=128), nullable=False),
    sa.Column('description', sa.String(length=1000), nullable=False),
    sa.Column('year', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['award_type_id'], [u'toolkit.award_type.id'], name=op.f('fk_award_award_type_id_award_type'), onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_award')),
    schema='toolkit'
    )
	op.create_table('award_nomination',
    sa.Column('nomination_id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('award_id', sa.Integer(), nullable=False),
    sa.Column('nominee_id', sa.Integer(), nullable=False),
    sa.ForeignKeyConstraint(['nominee_id'], [u'toolkit.award_nominee.nominee_id'], name=op.f('fk_award_nomination_nominee_id_award_nominee'), onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['award_id'], [u'toolkit.award.id'], name=op.f('fk_award_nomination_award_id_award'), onupdate='CASCADE', ondelete='CASCADE'),
    sa.PrimaryKeyConstraint('nomination_id', name=op.f('pk_award_nomination')),
    schema='toolkit'
    )
	op.create_table('award_nomination_status',
    sa.Column('id', sa.Integer(), autoincrement=True, nullable=False),
    sa.Column('actorcms_id', sa.Integer(), nullable=False),
    sa.Column('nomination_id', sa.Integer(), nullable=False),
    sa.Column('status', sa.String(length=128), nullable=False),
    sa.Column('actor_remarks', sa.String(length=1000), nullable=True),
    sa.Column('nominee_reason', sa.String(length=1000), nullable=True),
    sa.Column('awardee_citation', sa.String(length=1000), nullable=True),
    sa.Column('timestamp', sa.DateTime(), nullable=False),
    sa.ForeignKeyConstraint(['nomination_id'], [u'toolkit.award_nomination.nomination_id'], name=op.f('fk_award_nomination_status_id_award_nomination'), onupdate='CASCADE', ondelete='CASCADE'),
    sa.ForeignKeyConstraint(['actorcms_id'], [u'toolkit.person.cms_id'], name=op.f('fk_award_nomination_status_actorcms_id_person'), onupdate='CASCADE', ondelete='SET NULL'),
    sa.PrimaryKeyConstraint('id', name=op.f('pk_award_nomination_status')),
    schema='toolkit'
    )


def downgrade():
	op.drop_table('award_nominee', schema='toolkit')
	op.drop_table('award_category', schema='toolkit')
	op.drop_table('award_nomination', schema='toolkit')
	op.drop_table('award_status', schema='toolkit')

