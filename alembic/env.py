#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

import logging
import traceback

from alembic import context
from alembic.runtime.migration import MigrationContext, MigrationInfo
from sqlalchemy import engine_from_config, pool
from logging.config import fileConfig

import icms_orm

from icms_orm import DatabaseViewMixin
from icms_orm.common import DeclarativeBase

# this is the Alembic Config object, which provides
# access to the values within the .ini file in use.
config = context.config

# Interpret the config file for Python logging.
# This line sets up loggers basically.
fileConfig(config.config_file_name)
log: logging.Logger = logging.getLogger(__name__)
# add your model's MetaData object here
# for 'autogenerate' support
# from myapp import mymodel
# target_metadata = mymodel.Base.metadata
target_metadata = DeclarativeBase.metadata

# other values from the config, defined by the needs of env.py,
# can be acquired:
# my_important_option = config.get_main_option("my_important_option")
# ... etc.


def include_object(object, name, type_, reflected, compare_to):
    if type_ == 'table' and object.name.startswith('view_'):
        log.info('HACK-FILTERING OUT WHAT SEEMS TO BE A VIEW UNDER A TABLE\'S DISGUISE: {0}'.format(object.fullname))
        return False
    return True


def create_views(ctx: MigrationContext, step: MigrationInfo, heads, run_args):
    try:
        log.info('NOW THE VIEWS SHOULD BE (RE-)CREATED!')
        DatabaseViewMixin.drop_views(ctx.connection)
        DatabaseViewMixin.create_views(ctx.connection)
        sql_fix_permissions = f'GRANT SELECT, INSERT, UPDATE, REFERENCES ON ALL TABLES IN SCHEMA public to {icms_orm.icms_reader_role()}'
        ctx.execute(sql_fix_permissions)
        log.info(f'Access to public tables updated using «{sql_fix_permissions}»')
    except Exception as e:
        log.error(traceback.format_exc())
        log.warning('Failed to create DB views, continuing the migration though. Is {0} the very latest revision?'
                        .format(ctx.get_current_revision()))


def run_migrations_offline():
    """Run migrations in 'offline' mode.

    This configures the context with just a URL
    and not an Engine, though an Engine is acceptable
    here as well.  By skipping the Engine creation
    we don't even need a DBAPI to be available.

    Calls to context.execute() here emit the given string to the
    script output.

    """
    url = config.get_main_option("sqlalchemy.url")
    context.configure(url=url, target_metadata=target_metadata, literal_binds=True)

    with context.begin_transaction():
        context.run_migrations()


def run_migrations_online():
    """Run migrations in 'online' mode.

    In this scenario we need to create an Engine
    and associate a connection with the context.

    """
    connectable = engine_from_config(
        config.get_section(config.config_ini_section),
        prefix='sqlalchemy.',
        poolclass=pool.NullPool)

    with connectable.connect() as connection:
        context.configure(
            connection=connection,
            target_metadata=target_metadata,
            include_object=include_object,
            version_table='alembic_version',
            on_version_apply=[create_views]
        )

        with context.begin_transaction():
            context.run_migrations()


if context.is_offline_mode():
    run_migrations_offline()
else:
    run_migrations_online()
