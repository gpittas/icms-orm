from sqlalchemy import Enum
from icms_orm import PseudoEnum, cms_common_schema_name
from sqlalchemy.types import Integer, String, Text, DateTime, Boolean, Date, SmallInteger, LargeBinary
from sqlalchemy import Column, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import JSONB
import json
from datetime import datetime
import hashlib
from icms_orm.common.common_schema_base import CommonBaseMixin, DeclarativeBase


class AppNameValues(PseudoEnum):
    EPR = 'epr'
    TOOLS = 'tools'
    NOTES = 'notes'
    COMMON = 'common'
    CADI = 'cadi'


app_names_enum = Enum(*AppNameValues.values(), name='app_names_enum', schema=cms_common_schema_name(), metadata=DeclarativeBase.metadata)
# for backward-compatibility's sake
app_names = AppNameValues.values()


class UserPreferences(CommonBaseMixin, DeclarativeBase):
    """
    Class to host the user's preferences for each app
    """
    id = Column(Integer, primary_key=True, autoincrement=True)
    hrid = Column(Integer, nullable=False)
    app_name = Column(app_names_enum)
    prefs = Column(JSONB)
    lastupdate = Column(DateTime, default=datetime.utcnow, nullable=False)

    def __init__(self, hrid, app_name, prefs, lastupdate=None):
        self.hrid = hrid
        self.app_name = app_name
        self.prefs = prefs
        if lastupdate: self.lastupdate = lastupdate

    def to_json(self):
        return json.dumps({'id': self.id,
                           'hrId': self.hrid,
                           'appName': self.app_name,
                           'prefs': self.prefs,
                           'lastUpdate': str(self.lastupdate),
                           })


class EmailMessage(CommonBaseMixin, DeclarativeBase):

    id = Column(Integer, primary_key=True, autoincrement=True)
    sender = Column(String(256))
    reply_to = Column(String(256))
    to = Column(Text, nullable=False)
    cc = Column(Text)
    bcc = Column(Text)
    subject = Column(Text, nullable=False)
    body = Column(Text, nullable=False)
    # some meta-info
    hash = Column(String(32), nullable=False)
    source_application = Column(String(32))
    remarks = Column(Text)
    last_modified = Column(DateTime, nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)

    def __init__(self, sender, to, subject, body, reply_to=None, cc=None, bcc=None, source_app=None, remarks=None):
        self.sender = sender
        self.reply_to = reply_to
        self.to = to
        self.subject = subject
        self.body = body
        self.cc = cc
        self.bcc = bcc
        self.source_application = source_app
        self.remarks = remarks
        self.rehash()

    def rehash(self):
        self.hash = u''
        for field in (self.sender, self.subject, self.body):
            self.hash = hashlib.md5((self.hash + field).encode('utf-8')).hexdigest()

    def is_valid(self):
        return self.to is not None and self.sender is not None and self.body is not None and self.body.strip() != ''

    @classmethod
    def compose_message(cls, sender, to, subject, body, cc=None, bcc=None, source_app=None, reply_to=None,
                        remarks=None, db_session=None):
        email = cls(sender=sender, to=to, subject=subject, body=body, cc=cc, bcc=bcc, source_app=source_app,
                    remarks=remarks, reply_to=reply_to)
        log = EmailLog(email=email, action=EmailLog.Action.ENQUEUE, status=EmailLog.Status.NEW)
        if db_session:
            db_session.add(email)
            db_session.add(log)
            db_session.commit()
        return email, log


class EmailLog(CommonBaseMixin, DeclarativeBase):

    id = Column(Integer, primary_key=True, autoincrement=True)
    email_id = Column(Integer, ForeignKey(EmailMessage.id, ondelete='CASCADE'))
    email = relationship(EmailMessage, backref='history')
    timestamp = Column(DateTime, nullable=False, default=datetime.utcnow)
    action = Column(String(32), nullable=False)
    status = Column(String(32), nullable=False)
    details = Column(Text)

    def __init__(self, email, action, status, details=None):
        if isinstance(email, int):
            self.email_id = email
        else:
            self.email = email
        self.action = action
        self.status = status
        self.details = details

    class Action(object):
        ENQUEUE = 'ENQUEUE'
        SEND = 'SEND'
        CANCEL = 'CANCEL'

    class Status(object):
        NEW = 'NEW'
        SENT = 'SENT'
        FAILED = 'FAILED'
        CANCELLED = 'CANCELLED'


class MarkdownNote(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(64), nullable=False, unique=True)
    body = Column(Text)


class ImageUpload(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(64), nullable=False, unique=True)
    data = Column(LargeBinary, nullable=True)
    url = Column(Text, nullable=True)


class Announcement(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    type = Column(String(8), nullable=None, default='INFO')
    subject = Column(Text, nullable=False)
    content = Column(Text, nullable=False)
    start_datetime = Column(DateTime, nullable=False, default=datetime.utcnow)
    end_datetime = Column(DateTime, nullable=True, default=None)
    application = Column(app_names_enum, nullable=True, default=None)


class ApplicationAsset(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(64), unique=True, nullable=False)
    application = Column(app_names_enum)
    data = Column(JSONB)

    @classmethod
    def store(cls, name, value, application=None):
        obj = cls.session().query(ApplicationAsset).filter(ApplicationAsset.name == name).one_or_none()
        obj = obj or ApplicationAsset.from_ia_dict({ApplicationAsset.name: name})
        setattr(obj, ApplicationAsset.application.key, application)
        setattr(obj, ApplicationAsset.data.key, value)
        cls.session().add(obj)
        cls.session().commit()

    @classmethod
    def retrieve(cls, name):
        record = cls.session().query(ApplicationAsset.data).filter(ApplicationAsset.name == name).one_or_none()
        return record and record[0] or None


class RowOperationTypes(PseudoEnum):
    INSERT = 'insert'
    UPDATE = 'update'
    DELETE = 'delete'

row_opertaion_enum = Enum(*RowOperationTypes.values(), schema=cms_common_schema_name(), name='row_operation_type')

class RowChange(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    schema_name = Column(String(32), nullable=False)
    table_name = Column(String(64), nullable=False)
    primary_key = Column(Integer, nullable=True)
    composite_key = Column(JSONB, nullable=True)
    former_values = Column(JSONB)
    new_values = Column(JSONB)
    operation = Column(row_opertaion_enum, nullable=False)
    timestamp = Column(DateTime, nullable=False)
    cms_id = Column(Integer, nullable=True)