#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from icms_orm import PseudoEnum
from sqlalchemy.types import Integer, String, DateTime, Boolean, Date, SmallInteger
from sqlalchemy import Column, ForeignKey
from sqlalchemy.orm import relationship
from datetime import datetime
from icms_orm.common.common_schema_base import CommonBaseMixin, DeclarativeBase
from icms_orm.common.common_schema_people_tables import Person


class OrgUnitTypeName(PseudoEnum):
    """
    We don't create an enum here because Position entries will anyway be referenced by id.
    Also, alembic would probably go haywire with evolving enums.
    """
    BOARD = 'Board'
    COMMITTEE = 'Committee'
    OFFICE = 'Office'
    # Coordination separate from Coord. Area to capture the different status of Upgrade and Technical Coords vs the rest
    COORDINATION = 'Coordination'
    COORDINATION_AREA = 'Coordination Area'
    SUBDETECTOR = 'Subdetector'
    INSTITUTE = 'Institute'
    FUNDING_AGENCY = 'Funding Agency'
    POG = 'Physics Object Group'
    PAG = 'Physics Analysis Group'
    DPG = 'Detector Performance Group'
    GENERIC_UNIT_LEVEL_2 = 'Level 2 Entity'
    GENERIC_UNIT_LEVEL_3 = 'Level 3 Entity'
    EDITORIAL_BOARD = 'Editorial Board'
    # that one can perhaps encompass both the CB Adviosry Group and SP / TC Advisors. Are the latter a formal unit
    # or are they just by convention shown as a single box - to be found out.
    ADVISORS = 'Advisors'
    REGION = 'Region'


class PositionName(PseudoEnum):
    """
    We don't create an enum here because Position entries will anyway be referenced by id.
    Also, alembic would probably go haywire with evolving enums.
    """
    SPOKESPERSON = 'Spokesperson'
    SECRETARY = 'Secretary'
    CHAIRPERSON = 'Chairperson'
    DEPUTY = 'Deputy'
    MANAGER = 'Manager'
    COORDINATOR = 'Coordinator'
    MEMBER = 'Member'
    CONVENER = 'Convener'
    TECHNICAL_COORDINATOR = 'Technical Coordinator'
    RESOURCE_MANAGER = 'Resource Manager'
    LEADER = 'Leader'
    DEPUTY_LEADER = 'Deputy Leader'
    COMMUNICATIONS_PERSON = 'Communications Person'
    LINK_PERSON = 'Link Person'


class OrgUnitType(CommonBaseMixin, DeclarativeBase):
    id = Column(SmallInteger, primary_key=True, autoincrement=True)
    last_modified = Column(DateTime, nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)
    name = Column(String(64), nullable=False)   #, comment='eg. BOARD, COMMITTEE, DETECTOR SYSTEM, COORDINATION AREA...')
    level = Column(SmallInteger, nullable=True) #, comment='The lower the number, the higher its place in the chart.')
    is_enclosed = Column(Boolean, nullable=False, default=True)
    is_active = Column(Boolean, nullable=False, default=True)


class OrgUnit(CommonBaseMixin, DeclarativeBase):
    id = Column(SmallInteger, primary_key=True, autoincrement=True)
    last_modified = Column(DateTime, nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)
    type_id = Column(SmallInteger, ForeignKey(OrgUnitType.id, onupdate='CASCADE', ondelete='CASCADE'), nullable=False)
    type = relationship(OrgUnitType, lazy='joined')
    # Perhaps domain values could be constrained to some set of predefined values:
    # - an enum or a table periodically populated by a select with union across different source tables
    # On the other hand a varchar approach should not introduce a lot of duplication:
    # - only one management board, one authorship committee etc
    domain = Column(String(64), nullable=True)    #, comment='eg. COLLABORATION, MANAGEMENT, TRACKER, AUTHORSHIP, ENGAGEMENT...')
    superior_unit_id = Column(SmallInteger, ForeignKey(id, onupdate='CASCADE', ondelete='CASCADE'), nullable=True)
    superior_unit = relationship(lambda: OrgUnit, foreign_keys=[superior_unit_id])
    # Unit enclosed by another can be a subunit of the latter or just a logical construct describing a complex structure
    enclosing_unit_id = Column(SmallInteger, ForeignKey(id, onupdate='CASCADE', ondelete='CASCADE'), nullable=True)
    enclosing_unit = relationship(lambda: OrgUnit, foreign_keys=[enclosing_unit_id])
    # Outermost unit could be reached like so: while(unit.enclosing_unit is not None): unit = unit.enclosing_unit
    outermost_unit_id = Column(SmallInteger, ForeignKey(id, onupdate='CASCADE', ondelete='CASCADE'), nullable=True)
    outermost_unit = relationship(lambda: OrgUnit, foreign_keys=[outermost_unit_id])
    is_active = Column(Boolean, nullable=False, default=True)


OrgUnit.add_unique_key('unit_type_domain_key', OrgUnit.type_id, OrgUnit.domain)


class Position(CommonBaseMixin, DeclarativeBase):
    id = Column(SmallInteger, primary_key=True, autoincrement=True)
    last_modified = Column(DateTime, nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)
    name = Column(String(64), nullable=False)   #, comment='eg. CHAIRPERSON, MANAGER, COORDINATOR, DEPUTY, SECRETARY...')
    # because positions hierarchy (manager / convener / coordinator etc) might differ across unit types (boards)
    unit_type_id = Column(SmallInteger, ForeignKey(OrgUnitType.id, onupdate='CASCADE', ondelete='CASCADE'), nullable=False)
    unit_type = relationship(OrgUnitType, lazy='joined')
    level = Column(SmallInteger, nullable=True) #, comment='The lower the number, the higher the level')
    is_active = Column(Boolean, nullable=False, default=True)


Position.add_unique_key('position_name_unit_type_id', Position.name, Position.unit_type_id)


class Tenure(CommonBaseMixin, DeclarativeBase):
    id = Column(Integer, primary_key=True, autoincrement=True)
    last_modified = Column(DateTime, nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)
    cms_id = Column(Integer, ForeignKey(Person.cms_id, onupdate='CASCADE', ondelete='SET NULL'))
    position_id = Column(SmallInteger, ForeignKey(Position.id, onupdate='CASCADE', ondelete='CASCADE'), nullable=False)
    position = relationship(Position, lazy='joined')
    unit_id = Column(SmallInteger, ForeignKey(OrgUnit.id, onupdate='CASCADE', ondelete='CASCADE'), nullable=False)
    unit = relationship(OrgUnit, lazy='joined')
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)


class ExOfficioMandate(CommonBaseMixin, DeclarativeBase):
    """
    Describes the rules of ex-officio inclusions between different OrgUnits
    """
    id = Column(Integer, primary_key=True, autoincrement=True)
    last_modified = Column(DateTime, nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)
    src_unit_id = Column(SmallInteger, ForeignKey(OrgUnit.id, onupdate='CASCADE', ondelete='CASCADE'), nullable=False)
    src_unit = relationship(OrgUnit, foreign_keys=[src_unit_id])
    dst_unit_id = Column(SmallInteger, ForeignKey(OrgUnit.id, onupdate='CASCADE', ondelete='CASCADE'), nullable=False)
    dst_unit = relationship(OrgUnit, foreign_keys=[dst_unit_id])
    src_position_level = Column(SmallInteger, nullable=False)
    dst_position_level = Column(SmallInteger, nullable=False)
    src_position_name = Column(String(64), nullable=True)   #, comment='Can be used to express specific mapping of position in X to role in Y.')
    dst_position_name = Column(String(64), nullable=True)   #, comment='Can be used to express specific mapping of position in X to role in Y.')


class JobOpening(CommonBaseMixin, DeclarativeBase):
    id = Column(SmallInteger, primary_key=True, autoincrement=True)
    title = Column(String(200), nullable=False)
    description = Column(String(1000), nullable=False) # or mandate 
    requirements = Column(String(1000), nullable=False) # on the candidate
    start_date = Column(Date, nullable=False)
    end_date = Column(Date, nullable=True)
    deadline = Column(Date, nullable=False)
    position_id = Column(SmallInteger, ForeignKey(Position.id, onupdate='CASCADE'))
    position = relationship(Position, lazy='joined')
    status = Column(String(80), nullable=False)
    last_modified = Column(DateTime, nullable=False, default=datetime.utcnow, onupdate=datetime.utcnow)
