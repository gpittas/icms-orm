import logging
from typing import Type

from icms_orm import DatabaseViewMixin, epr_role_name, epr_bind_key
from icms_orm.epr.epr_schema_models import EprBaseMixin
from icms_orm import IcmsDeclarativeBasesFactory
from sqlalchemy.types import Integer, String, Text, DateTime, Boolean, Date, SmallInteger, LargeBinary, Float
from sqlalchemy import Column, DDL, event
from sqlalchemy.dialects.postgresql import JSONB
from sqlalchemy import DDL, event
from icms_orm.common.common_schema_people_tables import person_status_enum
from icms_orm.epr.epr_schema_models import TimeLineUser, Category


DeclarativeBase = IcmsDeclarativeBasesFactory.get_for_bind_key(epr_bind_key())


class EprSchemaViewBase(EprBaseMixin, DatabaseViewMixin):
    __bind_key__ = EprBaseMixin.__bind_key__
    __table_args__ = EprBaseMixin.__table_args__

    @classmethod
    def get_schema_name(cls) -> str:
        return cls.__table_args__.get('schema')

    @classmethod
    def get_role(cls):
        return epr_role_name()


class PeopleStatsView(EprSchemaViewBase, DeclarativeBase):
    __tablename__ = 'view_people_summary'
    __icms_dependencies__ = ['epr.time_line_user', 'epr.categories']

    year = Column(Integer, primary_key=True)
    activity_name = Column(String, primary_key=True)
    total = Column(Float)

    @classmethod
    def get_view_name(cls) -> str:
        return cls.__tablename__

    @classmethod
    def get_sql(cls) -> str:
        return  "select ttl as total, year, c.name as activity_name from (select sum(year_fraction) as ttl, year, category from epr.time_line_user tlu " + \
                "where tlu.status = 'CMS' " + \
                "group by year, category order by year desc, category) as stats " + \
                "left join epr.categories c on (stats.category = c.id)"


