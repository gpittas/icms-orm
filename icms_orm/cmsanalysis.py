#  Copyright (c) 2015-2019 CERN, Geneva, Switzerland
#  SPDX-License-Identifier: Apache-2.0

from typing import Type
from icms_orm.orm_interface.model_base_class import IcmsModelBase
from icms_orm import PseudoEnum
from sqlalchemy import Column, ForeignKey
from sqlalchemy.orm import relationship
from sqlalchemy.types import String, Integer, Text, SmallInteger
from icms_orm import cadi_bind_key, cadi_schema_name, IcmsDeclarativeBasesFactory
from type_decorators import StringDate
from .cmspeople import Person
import datetime


DeclarativeBase: Type[IcmsModelBase] = IcmsDeclarativeBasesFactory.get_for_bind_key(cadi_bind_key())


class CadiBaseMixin():
    __bind_key__ = cadi_bind_key()
    __table_args__ = {'schema': cadi_schema_name()}


class CMSDbException(Exception):
    def __init__(self, msg):
        self.msg = msg

    def __repr__(self):
        return f'<CMSDbException: {str(self.msg)}>'


class CadiAnalysis(CadiBaseMixin, DeclarativeBase):
    __tablename__ = 'Analysis'

    id = Column(Integer, primary_key=True)
    code = Column(String(255))
    name = Column(String(255))
    description = Column(String(255))
    sources = Column(String(255))
    samples = Column(String(255))
    remarks = Column(String(255))
    related = Column(String(255))
    URL = Column(String(255))
    status = Column(String(255))
    statusWeb = Column(String(255))
    targetDatePhyApp = Column(String(255))
    targetDatePreApp = Column(String(255))
    targetDatePub = Column(String(255))
    deadlineCWRcomments = Column(String(255))
    dateFinalReading = Column(String(255))
    publi = Column(String(255))
    PAS = Column(String(255))
    PAPER = Column(String(255))
    PAPERTAR = Column(String(255))
    Conference = Column(String(255))
    targetPubPeriod = Column(String(255))
    targetJournal = Column(String(255))
    targetConference = Column(Integer)
    keywords = Column(String(255))
    publicationStatus = Column(String(255))
    conferenceStatus = Column(String(255))
    type = Column(String(4))
    creator = Column(Integer)
    creatorName = Column(String(255))
    creatorDate = Column(String(255))
    updater = Column(Integer)
    updaterName = Column(String(255))
    updaterDate = Column(String(255))
    irc = Column(Integer)
    awg = Column(Integer)
    committee = Column(Integer)
    parentID = Column(Integer)
    preApprovalTalk = Column(String(255))
    approvalTalk = Column(String(255))
    HepData = Column(String(255))
    rivetVersion = Column(String(255))

    def __repr__(self):

        res = '<CadiAnalysis \n'
        for k in dir(self):
            if k.startswith('_'): continue
            try:
                res += '\t%s: "%s" \n' % (k, self.__dict__[k])
            except KeyError:
                pass
        res += ' />\n'
        return res

    def getSelectedData(self):
        code = self.code
        if code.startswith('d'): code = self.code[1:]
        cadiSelData = {
            'code': code,
            'targetDatePreApp': self.targetDatePreApp,
            'targetDatePub': self.targetDatePub,
            'Conference': self.Conference,
            'sources': self.sources,
            'samples': self.samples,
            'physWG': code[0:3],
            'name': self.name,
            'description': self.description,
            'creatorName': self.creatorName,
        }
        return cadiSelData

    def get_publication_info(self):
        """
        Extracts the data from "publi" column which looks like a kitchen sink for all that stuff.
        :return: a dictionary containing the data, like:
            {'arXiv': '1612.09159', 'doi': '10.1007/JHEP03(2017)162', 'journals': 'JHEP', 'cernid': 'CERN-EP-2016-296', 'cdsid': '2141101'}
        """
        return {k: v for k, v in [tuple(p.split('=')) for p in self.publi.split('+')]}

    def to_json(self):
        json = {
            'id'                  : self.id,
            'code'                : self.code,
            'name'                : self.name,
            'description'         : self.description,
            'sources'             : self.sources,
            'samples'             : self.samples,
            'remarks'             : self.remarks,
            'related'             : self.related,
            'URL'                 : self.URL,
            'status'              : self.status,
            'statusWeb'           : self.statusWeb,
            'targetDatePhyApp'    : self.targetDatePhyApp,
            'targetDatePreApp'    : self.targetDatePreApp,
            'targetDatePub'       : self.targetDatePub,
            'deadlineCWRcomments' : self.deadlineCWRcomments,
            'dateFinalReading'    : self.dateFinalReading,
            'publi'               : self.publi,
            'PAS'                 : self.PAS,
            'PAPER'               : self.PAPER,
            'PAPERTAR'            : self.PAPERTAR,
            'Conference'          : self.Conference,
            'targetPubPeriod'     : self.targetPubPeriod,
            'targetJournal'       : self.targetJournal,
            'targetConference'    : self.targetConference,
            'keywords'            : self.keywords,
            'publicationStatus'   : self.publicationStatus,
            'conferenceStatus'    : self.conferenceStatus,
            'type'                : self.type,
            'creator'             : self.creator,
            'creatorName'         : self.creatorName,
            'creatorDate'         : self.creatorDate,
            'updater'             : self.updater,
            'updaterName'         : self.updaterName,
            'updaterDate'         : self.updaterDate,
            'irc'                 : self.irc,
            'awg'                 : self.awg,
            'committee'           : self.committee,
            'parentID'            : self.parentID,
            'preApprovalTalk'     : self.preApprovalTalk,
            'approvalTalk'        : self.approvalTalk,
            'HepData'             : self.HepData,
            'rivetVersion'        : self.rivetVersion,

        }
        return json


class CadiAnalysisAnalysts(CadiBaseMixin, DeclarativeBase):
    __tablename__ = 'AnalysisAnalysts'

    id = Column(Integer, primary_key=True)
    cmsid = Column(String(100))
    name = Column(String(100))
    status = Column(String(10))
    startDate = Column(String(255))
    analysis = Column(Integer, ForeignKey(CadiAnalysis.id))
    analysisRef = relationship(CadiAnalysis, backref='analysts', lazy='joined')

    def __repr__(self):

        res = '<CadiAnalysisAnalysts \n'
        for k in dir(self):
            if k.startswith('_'): continue
            try:
                res += '\t%s: "%s" \n' % (k, self.__dict__[k])
            except KeyError:
                pass
        res += ' />\n'
        return res


class PaperAuthorStatusValues(PseudoEnum):
    ERR = 'ERR'
    IN = 'in'
    IN_NEW = 'in NEW'
    INT = 'int'
    INT_NEW = 'int NEW'
    OUT = 'out'
    OUT_NEW = 'out NEW'
    OUTT = 'outt'
    OUTT_NEW = 'outt NEW'
    OUTTT_NEW = 'outtt NEW'


class PaperAuthor(CadiBaseMixin, DeclarativeBase):
    __tablename__ = 'PaperAuthor'
    authorID = Column(Integer, nullable=False, primary_key=True)
    cmsid = Column(String(10), nullable=False)
    hrid = Column(String(10), nullable=False)
    code = Column(String(255), nullable=False)
    nameSignature = Column(String(50), nullable=False)
    status = Column(String(10), nullable=False)
    statusCms = Column(String(20), nullable=False)
    activity = Column(String(40), nullable=False)
    instCode = Column(String(40), nullable=False)
    instCodeNow = Column(String(40), nullable=False)
    instCodeAlso = Column(String(40), nullable=False)
    instCodeForced = Column(String(40), nullable=True)
    infn = Column(String(4), nullable=True)
    univOther = Column(String(4), nullable=True)
    inspireid = Column(String(40), nullable=True)
    name = Column(String(40), nullable=True)
    namf = Column(String(40), nullable=True)

    def toJson(self):
        json = {
            'authorID'       : self.authorID,
            'cmsid'          : self.cmsid,
            'hrid'           : self.hrid,
            'code'           : self.code,
            'nameSignature'  : self.nameSignature,
            'status'         : self.status,
            'statusCms'      : self.statusCms,
            'activity'       : self.activity,
            'instCode'       : self.instCode,
            'instCodeNow'    : self.instCodeNow,
            'instCodeAlso'   : self.instCodeAlso,
            'instCodeForced' : self.instCodeForced,
            'infn'           : self.infn,
            'univOther'      : self.univOther,
            'inspireid'      : self.inspireid,
            'name'           : self.name,
            'namf'           : self.namf,
        }
        return json

    def __repr__(self):
        info = self.toJson()
        outVal = '<PaperAuthor '
        for k,v in info.items():
            outVal += '%s:%s ' % (k,v)
        outVal += '>\n'
        return outVal


class PaperAuthorHistoryActions(PseudoEnum):
    ADD_MEMBER = 'AddMember'
    ADD_EXTERNAL = 'AddExternal'
    ADD_EXTERNALS = 'AddExternals'
    CHANGE_REQUEST = 'ChangeRequest'
    DELETE_MEMBER = 'DeleteMember'
    EXCEPTION_REQUEST = 'ExceptionRequest'
    INST_CODE_FIX = 'InstCodeFix'
    LIST_FILES_GEN = 'ListFilesGen'
    LIST_CLOSE = 'ListClose'
    LIST_GEN = 'ListGen'
    MANUAL = 'Manual'
    MANUAL_FIX = 'ManualFix'
    MEMBER_ALSO = 'MemberAlso'
    MEMBER_ALSO_PERM = 'MemberAlsoPERM'
    MEMBER_INFN = 'MemberInfn'
    MEMBER_INFN_PERM = 'MemberInfnPERM'
    MEMBER_NOW = 'MemberNow'
    MEMBER_NOW_PERM = 'MemberNowPERM'
    MEMBER_UNIV_OTHER = 'MemberUnivOther'
    MEMBER_UNIV_OTHER_PERM = 'MemberUnivOtherPERM'
    MOVE_MEMBER_IN = 'MoveMemberIn'
    MOVE_MEMBER_OUT = 'MoveMemberOut'
    SWAP_MEMBER_IN = 'SwapMemberIn'
    SWAP_MEMBER_OUT = 'SwapMemberOut'


class PaperAuthorHistory(CadiBaseMixin, DeclarativeBase) :
    __tablename__ = 'PaperAuthorHistory'
    historyID = Column(Integer, primary_key=True)
    cmsid = Column(String(10), nullable=False)
    hrid = Column(String(10), nullable=False)
    nicelogin = Column(String(255), nullable=False)
    code = Column(String(255), nullable=False)
    action = Column(String(255), nullable=False)
    instCode = Column(String(40), nullable=False)
    date = Column(StringDate(format='%d/%m/%Y', length=255), nullable=False)
    history = Column(String(255))

    def __init__(self, cmsId=None, code=None, action=None, history=None):
        person = None
        if cmsId:
            person = Person.query.filter_by(cmsId=cmsId).one()
        self.cmsid = cmsId
        self.hrid = person and person.hrId
        self.nicelogin = person and person.loginId
        self.code = code
        self.action = action
        self.instCode = person and person.instCode
        self.date = datetime.date.today().strftime('%d/%m/%Y')
        self.history = history

    def to_json(self):
        json = {'historyID': self.historyID,
                'cmsid': self.cmsid,
                'hrid': self.hrid,
                 'nicelogin' : self.nicelogin,
                 'code'      : self.code,
                 'action'    : self.action,
                 'instCode'  : self.instCode,
                 'date'      : self.date,
                 'history'   : self.history,
        }
        return json
    def __repr__( self ) :
        info = self.to_json( )
        outVal = '<PaperAuthor '
        for k, v in info.items( ) :
            outVal += '\t%s : %s \n' % (k, v)
        outVal += '>\n'
        return outVal


class CadiHistory(CadiBaseMixin, DeclarativeBase):
    __tablename__ = 'History'
    id = Column(Integer, primary_key=True, autoincrement=True)
    table = Column('tbl', String(255), nullable=False)
    masterId = Column('forId', Integer)
    updaterId = Column('updater', Integer)
    updaterName = Column('updaterName', String(255))
    updateDate = Column('updaterDate', String(255))
    description = Column('description', String(255))
    remarks = Column('remarks', String(255))
    field = Column('field', String(255))
    oldValue = Column('oldValue', String(255))
    newValue = Column('newValue', String(255))

    def __repr__( self ) :
        info = self.to_json( )
        outVal = '<CadiHistory '
        for k, v in info.items( ) :
            outVal += '\t%s : %s \n' % (k, v)
        outVal += '>\n'
        return outVal

    def to_json(self):
        json = { 'id' : self.id,
                 'table' : self.table,
                 'analysisId' : self.analysisId,
                 'analysis'    : self.analysis,
                 'updaterId'   : self.updaterId,
                 'updaterName' : self.updaterName,
                 'updateDate'  : self.updateDate,
                 'description' : self.description,
                 'remarks'     : self.remarks,
                 'field'       : self.field,
                 'oldValue'    : self.oldValue,
                 'newValue'    : self.newValue,
                 }
        return json

class ARC(CadiBaseMixin, DeclarativeBase):
    __tablename__ = 'Committee'
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    name = Column('name', String(255))
    creatorId = Column('creator', Integer)
    creatorName = Column('creatorName', String(255))
    createDate = Column('creatorDate', String(255))
    url = Column('URL', String(255))
    status = Column('status', String(255))
    updaterId = Column('updater', Integer)
    updaterName = Column('updaterName', String(255))
    updateDate = Column('updaterDate', String(255))
    remarks = Column('remarks', String(255))


class ARCMember(CadiBaseMixin, DeclarativeBase):
    __tablename__ = 'CommitteeMembers'
    id = Column('memberID', Integer, primary_key=True, autoincrement=True)
    cmsId = Column('cmsid', String(255))
    name = Column('name', String(100))
    status = Column('status', String(10))
    propstatus = Column('propstatus', String(10))
    startDate = Column('startDate', String(255))


class ARCsMembersRel(CadiBaseMixin, DeclarativeBase):
    __tablename__ = 'CommitteesMembersRel'
    id = Column('id', Integer, primary_key=True, autoincrement=False)
    memberId = Column('memberID', Integer, primary_key=True, autoincrement=False)


class IRC(CadiBaseMixin, DeclarativeBase):
    __tablename__ = 'Irc'
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    name = Column('name', String(255))
    instCode = Column('instCode', String(255))
    creatorId = Column('creator', Integer)
    creatorName = Column('creatorName', String(255))
    createDate = Column('creatorDate', String(255))
    status = Column('status', String(255))
    updaterId = Column('updater', Integer)
    updaterName = Column('updaterName', String(255))
    updateDate = Column('updaterDate', String(255))
    remarks = Column('remarks', String(255))


class IRCMembers(CadiBaseMixin, DeclarativeBase):
    __tablename__ = 'IrcReaders'
    id = Column('id', Integer, primary_key=True, autoincrement=True)
    cmsId = Column('cmsid', Integer)
    name = Column('name', String(255))
    status = Column('status', String(255))
    startDate = Column('startDate', String(255))
    irc = Column('irc', String(255))


class AWG(CadiBaseMixin, DeclarativeBase):
    __tablename__ = 'Awg'
    id = Column(Integer, primary_key=True, autoincrement=True)
    name = Column(String(255))
    type = Column(String(255))
    fullName = Column(String(255))
    creatorId = Column('creator', Integer)
    creatorName = Column(String(255))
    createDate = Column('creatorDate', String(255))
    url = Column('URL', String(255))
    forumUrl = Column('forumURL', String(255))
    status = Column(String(255))
    updaterId = Column('updater', Integer)
    updaterName = Column(String(255))
    updateDate = Column('updaterDate', String(255))
    remarks = Column(String(255))
    nextAnId = Column(String(255))


class AWGConvener(CadiBaseMixin, DeclarativeBase):
    __tablename__ = 'AwgConveners'
    id = Column(Integer, primary_key=True, autoincrement=True)
    cmsId = Column('cmsid', String(100))
    name = Column(String(100))
    status = Column(String(10))
    startDate = Column(String(255))
    awg = Column(Integer)


class CDSStepValues(PseudoEnum):
    CWR = 'CWR'
    FR = 'FR'
    SUB = 'SUB'


class CDSStatusValues(PseudoEnum):
    DONE = 'CDS_DONE'
    PENDING = 'CDS_PENDING'


class CDS(CadiBaseMixin, DeclarativeBase):
    __tablename__ = 'CDS'
    id = Column('PrKeyPF', Integer, primary_key=True, nullable=False, default=None, autoincrement=True)
    version = Column('version', String(255), nullable=False)
    step = Column('step', String(255), nullable=True, default=None)
    stepVersion = Column('stepversion', String(255), nullable=True, default=None)
    stepDate = Column('stepdate', String(255), nullable=True, default=None)
    cdsVersion = Column('cdsversion', String(255), nullable=True, default=None)
    record = Column('record', String(255), nullable=False)
    status = Column('status', String(255), nullable=False)
    code = Column('code', String(255), nullable=False)
    supersedes = Column('supersedes', String(255), nullable=True)
    title = Column('title', Text, nullable=False, default=None)
    abstract = Column('abstract', Text, nullable=False, default=None)
    note = Column('note', Text, nullable=False, default=None)
    subjects = Column('subjects', Text, nullable=False, default=None)
    keywords = Column('keywords', Text, nullable=False, default=None)
    cwrComments = Column('cwrcomments', String(255), nullable=False)
    enableComments = Column('enablecomments', String(5), nullable=False)
    file = Column('file', String(255), nullable=False)
    subfile = Column('subfile', String(255), nullable=True, default=None)
    mailfile = Column('mailfile', String(255), nullable=False)
    cdsRequestDate = Column('CDSrequestDate', String(255), nullable=False)
    cdsNotificationDate = Column('CDSnotificationDate', String(255), nullable=False)
    updater = Column('updater', SmallInteger, nullable=False, default=0)
    updaterName = Column('updaterName', String(255), nullable=False)
    updateDate = Column('updaterDate', String(255), nullable=False)
    updaterEmail = Column('updaterEmail', String(255), nullable=False)
    remarks = Column('remarks', Text, nullable=False, default=None)


class CDSLog(CadiBaseMixin, DeclarativeBase):
    __tablename__ = 'CDSLOG'
    id = Column('PrKeyPF', Integer, autoincrement=True, primary_key=True)
    action = Column('action', String(255))
    status = Column('status', String(255))
    step = Column('step', String(255))
    step_version = Column('stepversion', String(255))
    error = Column('error', String(255))
    remarks = Column('remarks', String(255))
    code = Column('code', String(255))
    record = Column('record', String(255))
    updater = Column('updater', SmallInteger, default=0)
    updated_by = Column('updaterName', String(255))
    update_time = Column('updaterDate', String(255))
    updater_email = Column('updaterEmail', String(255))


class Note(CadiBaseMixin, DeclarativeBase):
    __tablename__ = 'AnalysisNotes'
    id = Column('noteid', Integer, primary_key=True, autoincrement=True)
    cmsNoteId = Column(String(255))


class AnalysisNoteRel(CadiBaseMixin, DeclarativeBase):
    __tablename__= 'AnalysisNotesRel'
    id = Column('id', Integer, primary_key=True)
    noteId = Column('noteid', Integer, ForeignKey(Note.id), primary_key=True)
    note = relationship(Note)


# Just some aliases for convenience / ease / consistency (?)
Analysis = CadiAnalysis
AnalysisAnalysts = CadiAnalysisAnalysts
History = CadiHistory
