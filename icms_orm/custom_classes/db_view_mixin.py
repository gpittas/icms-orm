from icms_orm import IcmsDeclarativeBasesFactory
import logging
from sqlalchemy import event
from sqlalchemy.sql.ddl import DDL
from sqlalchemy.engine import Connection


def _db_view_visitor_fn(cls):
    cls.register_auto_creation_rectifier()


class DatabaseViewMixin(object):

    __icms_subclass_visit_function__ = _db_view_visitor_fn

    @classmethod
    def get_role(cls) -> str:
        return cls.get_schema_name

    @classmethod
    def get_schema_name(cls) -> str:
        return None

    @classmethod
    def get_view_name(cls) -> str:
        return None

    @classmethod
    def get_sql(cls) -> str:
        return None

    @classmethod
    def get_create_sql(cls):
        return ';'.join([
            'set role {0}'.format(cls.get_role()),
            'create view {schema}.{view_name} as {sql}'.format(schema=cls.get_schema_name(), view_name=cls.get_view_name(), sql=cls.get_sql()),
            'reset role'
        ])

    @classmethod
    def get_drop_sql(cls):
        return ';'.join([
            'set role {0}'.format(cls.get_role()),
            'drop view if exists {schema}.{view_name} cascade'.format(schema=cls.get_schema_name(), view_name=cls.get_view_name()),
            'reset role'
        ])

    @staticmethod
    def get_implementing_classes():
        _list = [DatabaseViewMixin]
        for _cls in _list:
            for _sub_cls in _cls.__subclasses__():
                _list.append(_sub_cls)
        return [_cls for _cls in _list if any([issubclass(_cls, _base) for _base in IcmsDeclarativeBasesFactory.base_classes()])]

    @staticmethod
    def create_views(connection: Connection, bind=None):
        statements = [_cls.get_create_sql() for _cls in DatabaseViewMixin.get_implementing_classes() if
                     issubclass(_cls, DatabaseViewMixin) and (bind is None or _cls.__table__.info.get('bind_key') == bind)
                    ]
        DatabaseViewMixin.__loop_until_all_execute_or_throw(connection, statements)

    @staticmethod
    def drop_views(connection: Connection, bind=None):
        statements = [_cls.get_drop_sql() for _cls in DatabaseViewMixin.get_implementing_classes() if
                      issubclass(_cls, DatabaseViewMixin) and (bind is None or _cls.__table__.info.get('bind_key') == bind)
                    ]
        DatabaseViewMixin.__loop_until_all_execute_or_throw(connection, statements)

    @staticmethod
    def __loop_until_all_execute_or_throw(connection: Connection, statements: [str]):
        while len(statements) > 0:
            failing_statements = []
            logging.info('Number of statements to execute: {0}'.format(len(statements)))
            first_exception = None
            for stmt in statements:
                try:
                    logging.debug('DB statement: {0}'.format(stmt))
                    for chunk in stmt.split(';'):
                        connection.execute(chunk)
                except Exception as e:
                    first_exception = first_exception or e
                    failing_statements.append(stmt)
                    connection.close()
            if len(statements) > len(failing_statements):
                statements = failing_statements
            else:
                if first_exception is not None:
                    raise first_exception
                else:
                    raise Exception('Looping with {0} statements left to execute'.format(len(statements)))
        logging.info("All statements executed!")

    @classmethod
    def register_auto_creation_rectifier(cls):
        """
        This method is here to automatically drop certain tables upon their no less automatic creation. 
        It will need to stay here until a better representation for views in SQLAlchemy is found.
        """
        logging.debug("REGISTERING DDL EVENT FOR CLASS {0}".format(cls.__name__))
        ddl_full = '; '.join([cls.get_drop_sql().replace(' view ', ' table '), cls.get_create_sql()])
        event.listen(cls.__table__, 'after_create', DDL(ddl_full))
        # Before dropping: remove the view, if exists and create a dummy table in that place so that Alchemy can happily remove it
        event.listen(cls.__table__, 'before_drop', DDL(cls.get_drop_sql()))
        event.listen(cls.__table__, 'before_drop', DDL('create table if not exists {schema}.{table}()'.format(schema=cls.get_schema_name(), table=cls.get_view_name())))
