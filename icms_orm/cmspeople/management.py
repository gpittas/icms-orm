from sqlalchemy import Column
from sqlalchemy.types import SmallInteger, Integer, Text
from type_decorators import *
from .common import PeopleBaseMixin, DeclarativeBase


class RoomRequest(PeopleBaseMixin, DeclarativeBase):
    __tablename__ = 'RoomRequest'

    id = Column('PrKeyPF', Integer, primary_key=True)
    year = Column('Year', StringInt(length=4))
    status = Column(String(length=50))
    cms_id_for = Column('CMSid', SmallInteger)
    cms_id_by = Column('fromCMSid', SmallInteger)
    cms_id_updater = Column('updaterCMSid', SmallInteger)
    date_request = Column('requestDate', StringDatetime(length=50, format='%Y/%m/%d'))
    date_update = Column('updateDate', StringDatetime(length=50, format='%Y/%m/%d'))
    date_accept = Column('acceptDate', StringDatetime(length=50, format='%Y/%m/%d'))
    date = Column('dates', StringDatetime(length=255, format='%A, %B %d, %Y'))
    time_slot = Column('hourSlot', String(255))
    cms_week = Column('cmsweek', String(255))
    conveners = Column(String(255))
    project = Column(String(20))
    title = Column(String(255))
    evo_flag = Column('EVO', String(6))
    pwd_falg = Column('PWD', String(6))
    password = Column('RegisterPWD', String(255))
    webcast_flag = Column('webCast', String(6))
    room_size = Column('roomSize', StringInt(length=50))
    official = Column('official', String(6))
    location = Column(String(255))
    preferred_location = Column('preference', String(255))
    remarks = Column(Text)
    reason = Column(Text)
    # this one is always empty, keep it out:
    # zaction
