from typing import Any, Dict, List, Type

from sqlalchemy.ext.declarative.api import declarative_base
import icms_orm
import logging
from icms_orm.orm_interface.model_base_class import IcmsModelBase
from icms_orm.orm_interface.model_metaclass import IcmsDeclarativeModelMetaclass
from sqlalchemy.sql.schema import MetaData

log: logging.Logger = logging.getLogger(__name__)


class IcmsDeclarativeBasesFactory(object):
    __naming_convention = {
        "ix": 'ix_%(column_0_label)s',
        "uq": "uq_%(table_name)s_%(column_0_name)s",
        "ck": "ck_%(table_name)s_%(constraint_name)s",
        "fk": "fk_%(table_name)s_%(column_0_name)s_%(referred_table_name)s",
        "pk": "pk_%(table_name)s"
    }

    __bases_by_bind_key: Dict[Any, Type[IcmsModelBase]] = dict()

    @classmethod
    def base_classes(cls) -> List[Type[IcmsModelBase]]:
        return list(cls.__bases_by_bind_key.values())

    @classmethod
    def bind_keys(cls) -> List[str]:
        return list(cls.__bases_by_bind_key.keys())

    @classmethod
    def get_default_schema_name_for_bind_key(cls, bind_key: str):
        for needle, result in {
            icms_orm.cms_common_bind_key(): icms_orm.cms_common_schema_name(),
            icms_orm.epr_bind_key(): icms_orm.epr_schema_name(),
            icms_orm.toolkit_bind_key(): icms_orm.toolkit_schema_name(),
            icms_orm.cms_people_bind_key(): icms_orm.cms_people_schema_name(),
        }.items():
            if isinstance(needle, str) and needle in (bind_key or ''):
                return result
        log.warning(
            'Could not determine the default schema for provided bind key: {0}'.format(bind_key))
        return None

    @classmethod
    def get_for_bind_key(cls, bind_key: str) -> Type[IcmsModelBase]:
        if bind_key not in cls.__bases_by_bind_key.keys():
            metadata = MetaData(schema=cls.get_default_schema_name_for_bind_key(
                bind_key), naming_convention=cls.__naming_convention)
            base = declarative_base(cls=IcmsModelBase, constructor=IcmsModelBase.__init__,
                                    metaclass=IcmsDeclarativeModelMetaclass, metadata=metadata)
            cls.__bases_by_bind_key[bind_key] = base
        return cls.__bases_by_bind_key[bind_key]
