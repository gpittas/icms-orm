import sys
from sqlalchemy.engine import url
import icms_orm


def for_mysql():
    print('-- SQL INIT SCRIPT FOR A DEV/TEST MYSQL DB')
    
    username = icms_orm.icms_legacy_user()
    host = icms_orm.mysql_host()
    print('create user `{legacy_user}`@`%`;'.format(legacy_user=username))
    for schema_name_getter in [
            icms_orm.cms_people_schema_name,
            icms_orm.old_notes_schema_name,
            icms_orm.old_notes_wf_schema_name,
            icms_orm.news_schema_name,
            icms_orm.cadi_schema_name,
            icms_orm.metadata_schema_name
    ]:
        print('drop database if exists {schema};'.format(
            schema=schema_name_getter()))
        print('create database {schema};'.format(schema=schema_name_getter()))
        print('grant all on {schema}.* to `{legacy_user}`@`%`;'.format(
            schema=schema_name_getter(), legacy_user=username))
    print('flush privileges;')


def for_postgres():
    _vars = {
        'role_tools': icms_orm.toolkit_role_name(),
        'role_epr': icms_orm.epr_role_name(),
        'role_icms': icms_orm.cms_common_role_name(),
        'schema_icms': icms_orm.cms_common_schema_name(),
        'schema_epr': icms_orm.epr_schema_name(),
        'schema_tools': icms_orm.toolkit_schema_name(),
        'role_reader': icms_orm.icms_reader_role()
    }
    print('-- SQL INIT SCRIPT FOR A DEV/TEST POSTGRES DB')
    print("CREATE ROLE serve NOSUPERUSER CREATEDB CREATEROLE LOGIN ENCRYPTED PASSWORD 'protect';")
    print("CREATE ROLE {role_tools} login encrypted password '{role_icms}' ROLE serve;".format(**_vars))
    print("CREATE ROLE {role_epr} login encrypted password '{role_icms}' ROLE serve;".format(**_vars))
    print("CREATE ROLE {role_icms} login encrypted password '{role_icms}' ROLE serve;".format(**_vars))
    # print("CREATE ROLE notes login encrypted password 'icms' ROLE serve;".format(**vars))
    # print("CREATE ROLE cadi login encrypted password 'icms' ROLE serve;".format(**vars))
    # print("-- Create needed schemas and set their ownership correspondingly".format(**vars))
    print("create database {role_icms} owner serve;".format(**_vars))
    print("\\c {role_icms};".format(**_vars))
    print("CREATE SCHEMA {schema_tools} AUTHORIZATION {role_tools};".format(**_vars))
    print("CREATE SCHEMA {schema_epr} AUTHORIZATION {role_epr};".format(**_vars))
    # print("CREATE SCHEMA notes AUTHORIZATION notes;".format(**vars))
    # print("CREATE SCHEMA cadi AUTHORIZATION cadi;".format(**vars))
    # print("-- 2.4.0 Create a generic icms_reader role".format(**vars))
    print("CREATE ROLE {role_reader};".format(**_vars))
    # print("-- 2.4.1 That can connect to icms database".format(**vars))
    print("GRANT CONNECT ON DATABASE {role_icms} to {role_reader};".format(**_vars))
    # print("-- 2.4.2 That can use all the schemas".format(**vars))
    print("GRANT USAGE ON SCHEMA {schema_epr} to {role_reader};".format(**_vars))
    print("GRANT USAGE ON SCHEMA {schema_tools} to {role_reader};".format(**_vars))
    print("GRANT USAGE ON SCHEMA public to {role_reader};".format(**_vars))
    print("-- 2.4.3 That can select from all tables in all schemas")
    print("GRANT SELECT ON ALL TABLES IN SCHEMA {schema_epr} to {role_reader};".format(**_vars))
    print("GRANT SELECT ON ALL TABLES IN SCHEMA {schema_tools} to {role_reader};".format(**_vars))
    print("GRANT SELECT, INSERT, UPDATE, REFERENCES ON ALL TABLES IN SCHEMA public to {role_reader};".format(**_vars))
    print("GRANT CREATE ON SCHEMA public to {role_reader};".format(**_vars))
    print("GRANT SELECT, USAGE ON ALL SEQUENCES IN SCHEMA public to {role_reader};".format(**_vars))
    print("-- 2.4.4 And that is a group role for schema owners")
    print("GRANT {role_reader} to {role_epr}, {role_tools}, {role_icms};".format(**_vars))
    print("-- 2.4.5 A sad hack: the icms role needs to be able to create views in the epr schema (at least as long as the migration scripts are distributed the way they are now, April 2020)")
    print("GRANT {role_epr} to {role_icms};".format(**_vars))


if __name__ == '__main__':
    args = {_a.lower() for _a in sys.argv}
    
    _dump_mysql = 'mysql' in args
    _dump_psql = 'psql' in args or 'postgres' in args

    if _dump_mysql or not _dump_psql:
        for_mysql()

    if _dump_psql or not _dump_mysql:
        for_postgres()
