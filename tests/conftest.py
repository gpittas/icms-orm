import os
import traceback
import icms_orm
import pytest
import subprocess
import logging

from sqlalchemy import MetaData
from icms_orm import OrmManager
from sqlalchemy.engine import url
from sqlalchemy.sql.schema import Table
from sqlalchemy.schema import MetaData
from sqlalchemy.engine.reflection import Inspector


class TestParameters(object):
    bind_key_getters = [
        icms_orm.cms_common_bind_key,
        icms_orm.epr_bind_key,
        icms_orm.toolkit_bind_key,
        icms_orm.cms_people_bind_key
    ]

    @staticmethod
    def bind_keys():
        """
        Returns the bind keys in the proper order of DB creation
        """
        return [_bkg() for _bkg in TestParameters.bind_key_getters]

    @staticmethod
    def bind_keys_dropwise():
        return reversed(TestParameters.bind_keys())


@pytest.fixture(scope='session')
def test_config():
    test_config = {
        'SQLALCHEMY_BINDS': {
            icms_orm.cms_people_bind_key(): 'mysql+pymysql://{legacy_user}@{mysql_host}/{old_people_schema}?charset=utf8'.format(
                legacy_user=icms_orm.icms_legacy_user(),
                old_people_schema=icms_orm.cms_people_schema_name(),
                mysql_host=icms_orm.mysql_host()
            ),
            icms_orm.toolkit_bind_key(): 'postgresql+psycopg2://{toolkit_role}:{icms_role}@{postgres_host}/{icms_role}'.format(
                toolkit_role=icms_orm.toolkit_role_name(),
                icms_role=icms_orm.cms_common_role_name(),
                postgres_host=icms_orm.postgres_host()
            ),
            icms_orm.epr_bind_key(): 'postgresql+psycopg2://{epr_role}:{icms_role}@{postgres_host}/{icms_role}'.format(
                epr_role=icms_orm.epr_role_name(), 
                icms_role=icms_orm.cms_common_role_name(),
                postgres_host=icms_orm.postgres_host()
            ),
            icms_orm.cms_common_bind_key(): 'postgresql+psycopg2://{icms_role}:{icms_role}@{postgres_host}/{icms_role}'.format(
                icms_role=icms_orm.cms_common_role_name(),
                postgres_host=icms_orm.postgres_host()
            )
        }
    }
    test_config['SQLALCHEMY_DATABASE_URI'] = test_config.get(
        'SQLALCHEMY_BINDS').get(icms_orm.cms_common_bind_key())

    return test_config


@pytest.fixture(scope='session')
def db_manager(test_config):
    # import something just to ensure that alchemy sees the table definitions
    from icms_orm.common import Affiliation
    from icms_orm.toolkit import AuthorshipApplicationCheck
    from icms_orm.cmsanalysis import Analysis
    from icms_orm.epr import TimeLineInst

    db = OrmManager(config=test_config)
    yield db


@pytest.fixture(scope='class')
def drop_create_all_databases(db_manager, test_config):
    for bind_key_to_drop in TestParameters.bind_keys_dropwise():
        OrmManager.drop_all(db_manager, bind=bind_key_to_drop)
    for bind_key_to_create in TestParameters.bind_keys():
        OrmManager.create_all(db_manager, bind_key_to_create)


